import mysql from "mysql2";

const connection = mysql
  .createPool({
    host: "localhost",
    user: "root",
    password: "root",
    database: "node",
  })
  .promise();

export const getCats = async (page, limit) => {
  const response = await connection.query("SELECT * FROM cats");

  const totalPages = Math.ceil(response[0].length / limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const result = {};
  result["page"] = page;
  result["limit"] = limit;
  result["total records"] = response[0].length;
  result["total pages"] = totalPages;
  result.records = response[0].slice(startIndex, endIndex);
  return result;
};

export const getCatWithId = async (id) => {
  const response = await connection.query(`SELECT * FROM cats WHERE id = ?`, [
    id,
  ]);
  return response[0][0];
};

export const addCat = async (name, age, breed) => {
  const response = await connection.query(
    `INSERT INTO cats (name,age,breed) VALUES(?,?,?)`,
    [name, age, breed]
  );
  const newId = response[0].insertId;
  return getCatWithId(newId);
};

export const deleteCat = async (id) => {
  const response = await connection.query(`DELETE FROM cats WHERE id = ?`, [
    id,
  ]);
  return response[0];
};

export const updateCat = async (name, age, breed, id) => {
  const response = await connection.query(
    `UPDATE cats SET name = ?, age = ?, breed = ? WHERE id = ?`,
    [name, age, breed, id]
  );
  return response[0];
};

export const queryCat = async (age_lte, age_gte) => {
  if (!age_lte && !age_gte) {
    return getCats();
  }
  if (age_lte && !age_gte) {
    const response = await connection.query(
      `SELECT * FROM cats WHERE age >= ?`,
      [age_lte]
    );
    return response[0];
  }
  if (!age_lte && age_gte) {
    const response = await connection.query(
      `SELECT * FROM cats WHERE age <= ?`,
      [age_gte]
    );
    return response[0];
  }
  const response = await connection.query(
    `SELECT * FROM cats WHERE age BETWEEN ? AND ?`,
    [age_lte, age_gte]
  );
  return response[0];
};
