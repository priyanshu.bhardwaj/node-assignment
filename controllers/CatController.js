import {
  getCats,
  getCatWithId,
  addCat,
  deleteCat,
  updateCat,
  queryCat,
} from "../database.js";

export const fetchCats = async (req, res) => {
  const page = req.query.page ? parseInt(req.query.page) : 1;
  const limit = req.query.limit ? parseInt(req.query.limit) : 5;

  if (page <= 0 || limit <= 0) {
    if (page <= 0) {
      return res.status(400).json({
        message: "Invalid page",
      });
    } else if (limit <= 0) {
      return res.status(400).json({
        message: "Invalid limit",
      });
    }
  }

  const response = await getCats(page, limit);
  res.send(response);
};

export const fetchCatWithId = async (req, res) => {
  const response = await getCatWithId(req.params.id);
  if (!response) {
    return res.status(400).json({
      message: "Not Found",
    });
  }
  res.send(response);
};

export const deleteById = async (req, res) => {
  const response = await deleteCat(req.params.id);
  const result = response.affectedRows;
  if (result === 0) {
    return res.status(400).json({
      message: "Not Found",
    });
  }
  res.status(200).json({
    message: "Delete Success",
  });
};

export const insertCat = async (req, res) => {
  const response = await addCat(req.body.name, req.body.age, req.body.breed);
  res.send(response);
};

export const editCat = async (req, res) => {
  const response = await updateCat(
    req.body.name,
    req.body.age,
    req.body.breed,
    req.params.id
  );
  const result = response.affectedRows;
  if (result === 0) {
    return res.status(400).json({
      message: "Not Found",
    });
  }
  res.status(200).json({
    message: "Update Success",
  });
};

export const searchCat = async (req, res) => {
  const response = await queryCat(
    Number(req.query.age_lte),
    Number(req.query.age_gte)
  );
  res.send(response);
};
