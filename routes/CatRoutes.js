import express from "express";
import {
  fetchCats,
  fetchCatWithId,
  deleteById,
  insertCat,
  editCat,
  searchCat,
} from "../controllers/CatController.js";

const router = express.Router();

//cat routes
router.get("/search", searchCat);

router.get("/", fetchCats);

router.get("/:id", fetchCatWithId);

router.put("/:id", editCat);

router.post("/", insertCat);

router.delete("/:id", deleteById);

export default router;
