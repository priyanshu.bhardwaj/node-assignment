import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import { validate } from "./middleware/Authentication.js";
import catRoutes from "./routes/CatRoutes.js";

dotenv.config();

const PORT = 8000;

const app = express();
app.use(cors());
app.use(express.json());
app.use(validate);

//routes
app.use("/cat", catRoutes);

app.listen(PORT, () => {
  console.log(`Server running on PORT ${PORT}`);
});
