import jwt from "jsonwebtoken";

export const validate = (req, res, next) => {
  let authHeader = req.headers.authorization;
  let token = authHeader?.split(" ")[1];

  if (token === undefined) {
    return res.status(401).json({ message: "Unauthorized access" });
  }

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err) => {
    if (err) {
      return res.status(403).json({ message: "Unauthorized access" });
    }
    next();
  });
};
